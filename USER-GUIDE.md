# User Guide

Before you get started, make sure you have installed `php` and `php_mbstring`. You'll also need `php-xml` if your markdown files contain HTML tags.

This program runs smoothly on PHP 8. Older versions are untested.

## 1. Prepare your source files

Create a directory to store the source files (markdown files, images, etc.) for your website.

### Static files

Anything not ending with `.md` will be copied into the target directory verbatim.

### Markdown files

For each `.md` file, you can insert the following lines (i.e. the front-matter section) at the start of the file:

```json
---
{
    "title": "Title of this page",
    "template": "template_to_use.php"
}
---
```

While having a front-matter section is NOT a requirement, you probably want to add one to specify something. When:

- `template` is not specified, `default.php` in the template directory will be used;
- `title` is not specified, the first level-1 heading (e.g. `# Foobar`) will be used; and if that doesn't exist either, file name will be a fallback.

A template can require more to be specified in the front-matter section. We'll explain that later.

## 2. Prepare a template

Create a directory to store the template files for you website. (Don't create one in the directory for source files!)

### HTML (and pre-processed PHP)

To create a new template, create a file named `whatever_you_want.php`, or simply `default.php` if you don't want to specify template name in each markdown file. Write in it whatever HTML code you'll need for a page. The following PHP variables are available:

- `$frontmatter`: used to access what you've specified in the front-matter section of a markdown file, e.g. `$frontmatter['title']`, `$frontmatter['date']`;
- `$src`: raw text of the markdown file;
- `$markdown`: `$src` but without the front-matter section;
- `$parsed`: parsed HTML code of the markdown file;
- `$dir_relative`: relative name of the directory where the file is located;
- `$basename`: file name (including the extension);
- and more that can be found in the source code.

*In case you're not familiar with PHP, you can use `<?= $var_name ?>` to print a variable in your template.*

### Assets (CSS, JS, etc.)

For assets such as CSS files, place them into the `assets` directory, and they'll be copied to the output directory verbatim.

### Init script

This is optional, but you can create a file named `init.php` which will be first executed every time you generate the site.

## 3. Generate the site

When everything is ready, run the following command to generate the site:

```sh
php peaksol-org-ssg.php \
    --template "/path/to/template_dir" \
    --input "/path/to/source_dir" \
    --output "/path/to/output_dir" \
    --clean
```

**Please note that the `--clean` option will remove all files in `/path/to/output_dir`!** Make sure you set the path correctly before executing the command. If you don't want to clean the output directory before generating, just remove that option.

You can save these lines into a .sh file and later run it as a script.
