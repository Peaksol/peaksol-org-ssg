<?php 
/*
 * Copyright (C) 2022-2024 Peaksol
 * For the full license information, view the COPYING file that was
 * distributed with this source code.
 */

$args = getopt('', ['template:', 'input:', 'output:', 'clean']);

if (!isset($args['template']) || !is_dir($args['template'])) {
	die('Argument "--template" is not specified or invalid.' . PHP_EOL);
}
$args['template'] = realpath($args['template']);

if (!isset($args['input']) || !is_dir($args['input'])) {
	die('Argument "--input" is not specified or invalid.' . PHP_EOL);
}
$args['input'] = realpath($args['input']);

if (!isset($args['output'])) {
	die('Argument "--output" is not specified.' . PHP_EOL);
}
if (!is_dir($args['output'])) mkdir($args['output']);
$args['output'] = realpath($args['output']);

if (isset($args['clean'])) {
	echo 'Cleaning output directory...' . PHP_EOL;
	foreach (new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($args['output'], RecursiveDirectoryIterator::SKIP_DOTS),
		RecursiveIteratorIterator::CHILD_FIRST
	) as $file) {
		if (is_file($file)) unlink($file);
		else rmdir($file);
	}
}

echo 'Loading dependencies...' . PHP_EOL;
require_once(__DIR__ . '/lib/Parsedown.php');
require_once(__DIR__ . '/lib/ParsedownExtra.php');
$parsedown = new ParsedownExtra();

/*
 * In this context, "dir" refers to a directory that is not a specific file (e.g. "/foo/bar/").
 * "path", on the contrary, refers to a specific file under a directory,
 * which is equivalent to dir followed by basename (e.g. "/foo/bar/index.md").
 */

function generate_file($dir_relative, $basename) {
	global $args;
	$path_relative = $dir_relative . '/' . $basename;
	$input_path = $args['input'] . '/' . $path_relative;
	$output_dir = $args['output'] . '/' . $dir_relative;

	if (pathinfo($basename, PATHINFO_EXTENSION) != 'md') {
		echo 'Copying "' . $path_relative . '"... ' . PHP_EOL;
		copy($input_path, $output_dir . '/' . $basename);
		return;
	}

	echo 'Parsing "' . $path_relative . '"...' . PHP_EOL;
	$src = file_get_contents($input_path);

	preg_match('/^(---\n\s*(.*?)\s*\n---\n)?\s*(# (.+?)\n)?/s', $src, $matches);
	$frontmatter = isset($matches[1]) ? json_decode($matches[2], true) : [];

	if (!isset($frontmatter['template']) || !file_exists($args['template'] .'/'. $frontmatter['template'])) {
		if (file_exists($args['template']) . '/default.php') $frontmatter['template'] = 'default.php';
		else {
			echo 'Skipping this file: template is unspecified or invalid, and default.php is not found.' . PHP_EOL;
			return;
		}
	}
	if (!isset($frontmatter['title'])) {
		if (isset($matches[4])) $frontmatter['title'] = $matches[4];
		else $frontmatter['title'] = ucwords(basename($dir_relative));
	}

	global $parsedown;
	$markdown = isset($matches[1]) ? substr($src, strlen($matches[1])) : $src;
	$parsed = $parsedown->text($markdown);

	ob_start();
	require($args['template'] . '/' . $frontmatter['template']);
	file_put_contents($output_dir . '/' . basename($basename, '.md') . '.html', ob_get_contents());
	ob_end_clean();
}

function generate_dir($input_dir) {
	global $args;
	$dir_relative = substr($input_dir, strlen($args['input']));

	if (!file_exists($input_dir . '/index.md')) {
		echo 'Skipped "' . $dir_relative . '": index.md not found.' . PHP_EOL;
		return;
	}

	$output_dir = $args['output'] . '/' . $dir_relative;
	if (!is_dir($output_dir)) mkdir($output_dir);
	/* parse index.md first */
	generate_file($dir_relative, 'index.md');

	foreach (glob($input_dir . '/*') as $file) {
		$basename = basename($file);
		if ($basename == '.' || $basename == '..' || $basename == 'index.md') continue;
		if (is_file($file)) generate_file($dir_relative, $basename);
		else generate_dir($file);
	}
}

if (file_exists($args['template'] . '/init.php')) {
	echo 'Loading initialization script...' . PHP_EOL;
	require($args['template'] . '/init.php');
}

if (is_dir($args['template'] . '/assets')) {
	echo 'Cloning template assets... ' . PHP_EOL;
	foreach (new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($args['template'] . '/assets', RecursiveDirectoryIterator::SKIP_DOTS),
	) as $file) {
		$path_relative = substr($file, strlen($args['template'] . '/assets'));
		$output_dir = $args['output'] . '/' . dirname($path_relative);
		if (!is_dir($output_dir)) mkdir($output_dir);
		copy($file, $args['output'] . '/' . $path_relative);	
	}
}

generate_dir($args['input']);

echo 'All done!' . PHP_EOL;
