# Peaksol's Static Site Generator

`peaksol-org-ssg`, regardless of the name, is a general-purpose static site generator. This program was originally designed solely for Peaksol's personal site, but is now capable of powering websites of your own.

## How to use

Please read [USER-GUIDE.md](USER-GUIDE.md).

## Example sites

- [Peaksol's Personal Site](https://peaksol.org/) ([source code](https://codeberg.org/Peaksol/peaksol-org));
- [Free Software Licenses Translated](https://licenses.peaksol.org/) ([source code](https://codeberg.org/Peaksol/licenses-translated));
- [Free Software Fans](https://fsfans.club) ([source code](https://codeberg.org/fsfans-cn/www));
- [Learn Free Software](https://learn.fsfans.club) ([source code](https://codeberg.org/Peaksol/learnfreesoftware)).

## License

This program is licensed under the MIT License. See [COPYING](COPYING) for copyright and license notices.
